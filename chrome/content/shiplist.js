// vim: set ts=8 sw=4 sts=4 ff=dos :

Components.utils.import("resource://kancolletimermodules/httpobserve.jsm");

var ShipList = {
    createShipOrganizationList: function(){
	// 艦隊編成
	let fleets = KanColleDatabase.deck.list();
	for( let j=0; j < fleets.length; j++ ){
	    let fleet = KanColleDatabase.deck.get(fleets[j]);

	    let rows = $('fleet-'+fleet.api_id);
	    RemoveChildren(rows);

	    for( let i=0; fleet.api_ship[i]!=-1 && i<6; i++){
		let row = CreateElement('row');
		let data = FindOwnShipData( fleet.api_ship[i] );
		let masterdata = FindShipData( fleet.api_ship[i] );
		if (!masterdata)
		    continue;
		row.appendChild( CreateLabel(KanColleDatabase.masterStype.get(masterdata.api_stype).api_name,'') );
		row.appendChild( CreateLabel(masterdata.api_name) );
		row.appendChild( CreateListCell( data.api_nowhp + "/" + data.api_maxhp) );
		row.appendChild( CreateLabel(""+data.api_cond) );

		let maxhp = parseInt(data.api_maxhp, 10);
		let nowhp = parseInt(data.api_nowhp, 10);
		if( nowhp <= maxhp*0.25 ){
		    row.style.backgroundColor = '#ff8080';
		}else{
		    row.style.backgroundColor = '';
		}
		rows.appendChild( row );
	    }

	}
    },

    setTitle: function(){
	let now = GetCurrentTime();

	// 艦艇リスト
	let ships = KanColleDatabase.ship.list();

	document.title = "保有艦艇リスト "+ships.length+"隻 ("+GetDateString(now*1000)+")";
    },

    init: function(){
	this.setTitle();
	this.createShipOrganizationList();

	KanColleTimerShipTableInit();
	KanColleTimerShipTableStart();

	KanColleDatabase.ship.appendCallback(this.setTitle);
	KanColleDatabase.ship.appendCallback(this.createShipOrganizationList);
	KanColleDatabase.deck.appendCallback(this.createShipOrganizationList);

	WindowOnTop( window, $('window-stay-on-top').hasAttribute('checked') );
    },

    destroy: function(){
	KanColleDatabase.deck.removeCallback(this.createShipOrganizationList);
	KanColleDatabase.ship.removeCallback(this.createShipOrganizationList);
	KanColleDatabase.ship.removeCallback(this.setTitle);

	KanColleTimerShipTableStop();
	KanColleTimerShipTableExit();
    }
};


window.addEventListener("load", function(e){
    ShipList.init();
}, false);
window.addEventListener("unload", function(e){
    ShipList.destroy();
}, false);
