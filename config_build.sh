#!/bin/bash

# Build config for the build script, build.sh. Look there for more info.

APP_NAME=kancolletimer
CHROME_PROVIDERS="chrome"
CLEAN_UP=1
ROOT_FILES="README license.txt"
ROOT_DIRS="defaults modules"
BUILD_CHECK=./cleancheck.sh
BEFORE_BUILD=
AFTER_BUILD=
